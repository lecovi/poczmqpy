"""This module is for testing clients with ZMQ"""
import json
import os
import random
import time
import uuid

import zmq


SERVER_ADDRESS = os.environ.get('SERVER_ADDRESS', "localhost")
SERVER_PORT = os.environ.get('SERVER_PORT', "4386")
DELAY = float(os.environ.get('DELAY', "0.03"))

RED = '\033[91m'
GREEN = '\033[92m'
BLUE = '\033[94m'
RESET = '\033[0m'

ctx = zmq.Context()
socket = ctx.socket(zmq.REQ)
socket.connect(f"tcp://{SERVER_ADDRESS}:{SERVER_PORT}")

VALUES = ["card", "turn", "unknown"]
TURNSTILE_ID = str(uuid.uuid4())

while True:
    value = random.choice(VALUES)
    msg = {
        "cmd": value,
        "tid": TURNSTILE_ID,
    }

    socket.send_multipart([json.dumps(msg).encode()])
    print(f"Sending {value}")

    response = socket.recv_multipart()
    response = response[0].decode()
    print(f"  Response from server: {response}")
    if "card" in response:
        print(f"{GREEN}  Card accepted!{RESET}")
    elif "turn" in response:
        print(f"{BLUE}  Turn accepted!{RESET}")
    else:
        print(f"{RED}  Unknown!{RESET}")

    print("  Restarting loop...")
    time.sleep(DELAY)