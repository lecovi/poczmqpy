"""This module is for testing server with ZMQ"""
import json
import os
import random
import time
import uuid

import zmq


SERVER_ADDRESS = "*"
SERVER_PORT = os.getenv('SERVER_PORT', "4386")
SERVER_URI = f"tcp://{SERVER_ADDRESS}:{SERVER_PORT}"
DELAY = float(os.getenv('DELAY', "0.25"))


ctx = zmq.Context()
socket = ctx.socket(zmq.REP)
print(f"Binding to {SERVER_URI} - Delay: {DELAY}")
socket.bind(SERVER_URI)

start_time = time.time()
elapsed_time = 0
recieved_messages = 0
clients = set()

try:
    while True:
        #  Wait for next request from client
        message = socket.recv_multipart()[0].decode()
        message = json.loads(message)
        print(f"Received request: {message}")
        recieved_messages += 1
        if message["tid"] not in clients:
            clients.add(message["tid"])

        if "card" in message["cmd"]:
            response_value = random.choice(["OK", "BAD"])
        elif "turn" in message["cmd"]:
            response_value = "TURN DETECTED"
        else:
            response_value = "UNKNOWN COMMAND"

        time.sleep(DELAY)
        response = {"cmd": message["cmd"], "value": response_value}
        socket.send_multipart([json.dumps(response).encode()])
        print(f"Sending response: {response}")

except KeyboardInterrupt:
    print("\nExiting...")
    elapsed_time = time.time() - start_time
    print(f"Received {recieved_messages} messages from {len(clients)} client in {elapsed_time:.2f} seconds")
    socket.close()
    ctx.term()
    exit()
