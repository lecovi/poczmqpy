# Poczmqpy

![Python](https://img.shields.io/badge/python-3-success?logo=python&style=flat-square&logoColor=white)

A simple project to test some features of ZMQ in Python.

## Description
This project aims to test some patterns of ZMQ in Python. It is a simple that tests the following features:

- REQ/REP

## Installation

You need to have `docker` and `docker compose` installed.

### Local env

Create a new virtualenv and install `pyzmq`:

```bash
python3 -m venv venv
source venv/bin/activate
python -m pip install pyzmq
```

## Usage

Server will listen in `tcp://*:4386` and clients will connect to `tcp://{SERVER_ADDRESS}:4386`.
By default the compose file will create 100 clients. You can override this using `--scale turnstile=NUMBER` in the `docker compose up` command.
You can set `DELAY` into `.env` to change the time between messages.

```bash
docker compose build
docker compose up -d
docker compose exec controller python controller/server.py
```

## Robust Reliable Queuing (Paranoid Pirate Pattern) 

Router will listen in `tcp://*:5555` for clients and `tcp://*:5556` for workers.
You can set `DELAY` into `.env` to change the time between messages.

Before "change" the client uncommenting command line in tunrstile service in `docker-compose.yml`
file.

```bash
docker-compose build
docker-compose up -d --scale turnstile=0

# run the router
docker-compose exec controller bash
python controller/ppqueue.py

# run the workers
docker compose exec controller bash
python controller/ppworkers.py

# run the turnstiles
docker-compose up -d
```

## Authors and acknowledgment

- Leandro E. Colombo Viña <colomboleandro at gmail.com>
- Marcos Mesmer y Rosset <marcosdmyr at gmail.com>
- Hernán Brunacci <hbrunacci at gmail.com>

## License

Check the [LICENSE](LICENSE) file.
